//
//  GameViewDelegate.swift
//  d06
//
//  Created by Quentin ROULON on 10/9/18.
//  Copyright © 2018 Quentin ROULON. All rights reserved.
//

import Foundation

protocol GameViewDelegate: NSObjectProtocol {
    func removeItem(view: GameView)
    func addItem(view: GameView)
    func updateItem(view: GameView)
}
