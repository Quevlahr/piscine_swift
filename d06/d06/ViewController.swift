//
//  ViewController.swift
//  d06
//
//  Created by Quentin ROULON on 10/9/18.
//  Copyright © 2018 Quentin ROULON. All rights reserved.
//

import UIKit

class ViewController: UIViewController, GameViewDelegate {

    var dynamicAnimator: UIDynamicAnimator?
    let gravityBehavior = UIGravityBehavior()
    let collisionBehavior = UICollisionBehavior()
    let elasticityBehavior = UIDynamicItemBehavior()
    
    @IBAction func panGestureAction(_ sender: UIPanGestureRecognizer) {
        if let currentGameView = sender.view?.subviews.first as? GameView? {
            currentGameView?.center = sender.location(in: self.view)
        }
    }
    
    func removeItem(view: GameView) {
        gravityBehavior.removeItem(view)
//        elasticityBehavior.removeItem(view)
//        collisionBehavior.removeItem(view)
        
        dynamicAnimator?.updateItem(usingCurrentState: view)
    }
    
    func addItem(view: GameView) {
        gravityBehavior.addItem(view)
//        elasticityBehavior.addItem(view)
//        collisionBehavior.addItem(view)
        
        dynamicAnimator?.updateItem(usingCurrentState: view)
    }
    
    func updateItem(view: GameView) {
        dynamicAnimator?.updateItem(usingCurrentState: view)
    }
    
    
    @IBAction func tapGestureAction(_ sender: UITapGestureRecognizer) {
        
        let squareLocation = sender.location(in: self.view)
        let gameView = GameView(delegate: self,
                                frame: CGRect(x: squareLocation.x - 50,
                                              y: squareLocation.y - 50,
                                              width: 100,
                                              height: 100))
        
        self.view.addSubview(gameView)
        gravityBehavior.addItem(gameView)
        collisionBehavior.addItem(gameView)
        elasticityBehavior.addItem(gameView)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        dynamicAnimator = UIDynamicAnimator(referenceView: self.view)
        
        gravityBehavior.magnitude = 1
        collisionBehavior.translatesReferenceBoundsIntoBoundary = true
        elasticityBehavior.elasticity = 0.5
        
        dynamicAnimator!.addBehavior(gravityBehavior)
        dynamicAnimator!.addBehavior(collisionBehavior)
        dynamicAnimator!.addBehavior(elasticityBehavior)
    }
}

