//
//  GameView.swift
//  d06
//
//  Created by Quentin ROULON on 10/9/18.
//  Copyright © 2018 Quentin ROULON. All rights reserved.
//

import UIKit

class GameView: UIView {

    weak var delegate: GameViewDelegate?
    let firstRand = arc4random_uniform(100)
    var initialCenter = CGPoint()

    @objc func scaleGesture(_ gesture: UIPinchGestureRecognizer) {
        guard let currentView = gesture.view else { return }
        
        switch gesture.state {
        case .began:
            currentView.transform = currentView.transform.scaledBy(x: gesture.scale, y: gesture.scale)
            gesture.scale = 1
            delegate!.removeItem(view: self)
        case .changed:
            currentView.transform = currentView.transform.scaledBy(x: gesture.scale, y: gesture.scale)
            gesture.scale = 1
            delegate!.updateItem(view: self)
        case .ended:
            delegate!.addItem(view: self)
        case .cancelled, .failed, .possible:
            break
        }
    }
    
    @objc func panGesture(_ gesture: UIPanGestureRecognizer) {
        guard let currentView = gesture.view else { return }

        let translation = gesture.translation(in: currentView.superview)
        
        switch gesture.state {
        case .began:
            delegate!.removeItem(view: self)
            self.initialCenter = currentView.center
        case .changed:
            currentView.center = CGPoint(x: initialCenter.x + translation.x, y: initialCenter.y + translation.y)
            delegate!.updateItem(view: self)
        case .ended:
            delegate!.addItem(view: self)
        case .cancelled, .failed, .possible:
            break
        }
    }
    
    override public var collisionBoundsType: UIDynamicItemCollisionBoundsType {
        return (firstRand % 2) == 0 ? .ellipse : .rectangle
    }
    
    init(delegate: GameViewDelegate?, frame: CGRect) {
        self.delegate = delegate
        super.init(frame: frame)
        
        if firstRand % 2 == 0 {
            self.layer.cornerRadius = 50
        }
        
        self.backgroundColor = UIColor(red: (CGFloat(firstRand) / 100),
                                       green: (CGFloat(arc4random_uniform(100)) / 100),
                                       blue: (CGFloat(arc4random_uniform(100)) / 100),
                                       alpha: 1)
        
        let panGesture = UIPanGestureRecognizer(target: self, action: #selector(panGesture(_:)))
        let pinchGesture = UIPinchGestureRecognizer(target: self, action: #selector(scaleGesture(_:)))
        
        self.addGestureRecognizer(panGesture)
        self.addGestureRecognizer(pinchGesture)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
