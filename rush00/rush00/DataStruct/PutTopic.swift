//
//  PutTopic.swift
//  rush00
//
//  Created by Quentin ROULON on 10/7/18.
//  Copyright © 2018 Quentin ROULON. All rights reserved.
//

import Foundation

struct PutTopic: Codable {
    init(name: String, kind: String) {
        topic = TopicTopic(
            name: name,
            kind: kind
        )
    }

    struct TopicTopic: Codable {
        // tags : Piscine iOS Swift / Piscine Swift iOS
        let tag_ids: [String] = ["574", "578"]
        let name: String
        let kind: String
        // language Français
//        let language_id: String = "1"
        // cursus 42
        let cursus_ids: [String] = ["1"]
    }
    
    let topic: TopicTopic
}
