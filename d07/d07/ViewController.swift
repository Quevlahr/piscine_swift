//
//  ViewController.swift
//  d07
//
//  Created by Quentin ROULON on 10/10/18.
//  Copyright © 2018 Quentin ROULON. All rights reserved.
//

import UIKit
import ForecastIO
import RecastAI

class ViewController: UIViewController, UITextFieldDelegate {

    /***************
     *   VARIABLES
     ***************/
    
    var botClient: RecastAIClient?
    var tokenRecast: String = "d33cd5e70b1b7205ec73479d5147c87a"
    var weatherClient: DarkSkyClient?
    
    /***************
     *   OUTLETS
     ***************/
    
    @IBOutlet weak var responseOutlet: UILabel!
    @IBOutlet weak var requestOutlet: UITextField! {
        didSet {
            self.requestOutlet.delegate = self
        }
    }
    @IBOutlet weak var senderOutlet: UIButton!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    /***************
     *   ACTIONS
     ***************/
    
    @IBAction func sendButton(_ sender: UIButton) {
        if let request = requestOutlet.text, request != "" {
            self.toggleView()
            
            getRecastLocation(request: requestOutlet.text!)
        }
    }
    
    /***************
     *   FUNCTIONS
     ***************/
    
    func getRecastLocation(request: String) {
        self.botClient?.textRequest(request,
            successHandler: { (response: Response) -> Void in
                guard let location = response.get(entity: "location"),
                    let longitude = location["lng"] as? Double,
                    let latitude = location["lat"] as? Double else {
                        self.toggleView()
                        self.responseOutlet.text = "Location not found"
                        return
                }

                print("location", location)
                self.weatherClient?.getForecast(latitude: latitude, longitude: longitude) { result in
                    switch result {
                    case .success(let currentForecast, _):
                        DispatchQueue.main.async {
                            self.responseOutlet.text = currentForecast.currently?.summary
                            self.toggleView()
                        }
                    case .failure( _):
                        self.toggleView()
                        self.responseOutlet.text = "Weather not found"
                    }
                }
            }
            
            , failureHandle: { (error: Error) -> Void in
                self.toggleView()
                self.responseOutlet.text = "Location not found"
               print("error", error.localizedDescription)
            }
        )
    }

    func toggleView() {
        if senderOutlet.isEnabled {
            senderOutlet.isEnabled = false
            responseOutlet.isHidden = true
            activityIndicator.startAnimating()
        } else {
            senderOutlet.isEnabled = true
            responseOutlet.isHidden = false
            activityIndicator.stopAnimating()
        }
    }
    
    /**************
    *   CLAVIER
    ***************/
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    @objc func keyboardWillChange(notification: Notification) {
        guard let keyboardRect = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue else {
            return
        }
        
        if notification.name == NSNotification.Name.UIKeyboardWillShow ||
            notification.name == NSNotification.Name.UIKeyboardWillChangeFrame {
            view.frame.origin.y = -(keyboardRect.height / 2)
        } else {
            view.frame.origin.y = 0
        }
        print("keyboard will change: \(notification.name.rawValue)")
    }
    
    /***************
     *   OVERRIDE
     ***************/
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.weatherClient = DarkSkyClient(apiKey: "abfe05946c829bec971a6131ff9f3885")
        self.weatherClient?.units = .si
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillChange(notification:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillChange(notification:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillChange(notification:)), name: NSNotification.Name.UIKeyboardWillChangeFrame, object: nil)
        
        self.botClient = RecastAIClient(token : tokenRecast)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "segueToConversation" {
            if let controller = segue.destination as? MessagesViewController {
                controller.botClient = self.botClient
                controller.weatherClient = self.weatherClient
            }
        }
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillChangeFrame, object: nil)
    }
}

