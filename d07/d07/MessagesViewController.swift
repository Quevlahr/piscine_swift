//
//  JSQMessagesViewController.swift
//  d07
//
//  Created by Quentin ROULON on 10/10/18.
//  Copyright © 2018 Quentin ROULON. All rights reserved.
//

import UIKit
import JSQMessagesViewController
import ForecastIO
import RecastAI

struct User {
    let id: String
    let name: String
}

class MessagesViewController: JSQMessagesViewController {

    /***************
     *   VARIABLES
     ***************/
    
    let currentUser = User(id: "1", name: "Quentin")
    let botUser = User(id: "2", name: "Bot")
    var messages = [JSQMessage]()
    
    var botClient: RecastAIClient?
    var weatherClient: DarkSkyClient?
    
    /***************
     *   FUNCTIONS
     ***************/
    
//    func getMessages() -> [JSQMessage] {
//        var messages = [JSQMessage]()
//
//        var message = JSQMessage(senderId: currentUser.id, displayName: currentUser.name, text: "Hello this is a test")
//        messages.append(message!)
//        message = JSQMessage(senderId: botUser.id, displayName: botUser.name, text: "Hi test !")
//        messages.append(message!)
//
//        return messages
//    }
    
    func getRecastLocation(request: String) {
        self.botClient?.textRequest(request,
            successHandler: { (response: Response) -> Void in
                guard let location = response.get(entity: "location"),
                    let longitude = location["lng"] as? Double,
                    let latitude = location["lat"] as? Double else {
                        let message = JSQMessage(senderId: self.botUser.id, displayName: self.botUser.name, text: "Location not found")
                        self.messages.append(message!)
                        self.finishSendingMessage()
                        return
                }
                
                print("location", location)
                self.weatherClient?.getForecast(latitude: latitude, longitude: longitude) { result in
                    switch result {
                    case .success(let currentForecast, _):
                        DispatchQueue.main.async {
                            let message = JSQMessage(senderId: self.botUser.id, displayName: self.botUser.name, text: "The weather is " + (currentForecast.currently?.summary)!)
                            self.messages.append(message!)
                            self.finishSendingMessage()
                        }
                    case .failure( _):
                        let message = JSQMessage(senderId: self.botUser.id, displayName: self.botUser.name, text: "Weather not found")
                        self.messages.append(message!)
                        self.finishSendingMessage()
                    }
                }
            }
            
            , failureHandle: { (error: Error) -> Void in
                let message = JSQMessage(senderId: self.botUser.id, displayName: self.botUser.name, text: "Location not found")
                self.messages.append(message!)
                self.finishSendingMessage()

            }
        )
    }
    
    /***************
     *   PROTOCOLS
     ***************/
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return messages.count
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, messageDataForItemAt indexPath: IndexPath!) -> JSQMessageData! {
        return messages[indexPath.row]
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, messageBubbleImageDataForItemAt indexPath: IndexPath!) -> JSQMessageBubbleImageDataSource! {
        let bubbleFactory = JSQMessagesBubbleImageFactory()
        
        let message = messages[indexPath.row]
        if currentUser.id == message.senderId {
            return bubbleFactory?.outgoingMessagesBubbleImage(with: .gray)
        } else {
            return bubbleFactory?.incomingMessagesBubbleImage(with: UIColor.lightGray)
        }
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, avatarImageDataForItemAt indexPath: IndexPath!) -> JSQMessageAvatarImageDataSource! {
        return nil
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, attributedTextForMessageBubbleTopLabelAt indexPath: IndexPath!) -> NSAttributedString! {
        let message = messages[indexPath.row]
        let messageUserName = message.senderDisplayName
        
        return NSAttributedString(string: messageUserName!)
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, layout collectionViewLayout: JSQMessagesCollectionViewFlowLayout!, heightForMessageBubbleTopLabelAt indexPath: IndexPath!) -> CGFloat {
        return 15
    }
    
    override func didPressSend(_ button: UIButton!, withMessageText text: String!, senderId: String!, senderDisplayName: String!, date: Date!) {
        self.getRecastLocation(request: text)
        let message = JSQMessage(senderId: senderId, displayName: senderDisplayName, text: text)
        messages.append(message!)
        
        finishSendingMessage()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.senderId = currentUser.id
        self.senderDisplayName = currentUser.name
//        messages =  self.getMessages()
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
