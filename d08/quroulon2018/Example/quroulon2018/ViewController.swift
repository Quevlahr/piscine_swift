//
//  ViewController.swift
//  quroulon2018
//
//  Created by Quevlahr on 10/11/2018.
//  Copyright (c) 2018 Quevlahr. All rights reserved.
//

import UIKit
import quroulon2018

@available(iOS 10.0, *)
class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let articleManager = ArticleManager()
        
        var articles: [Article] = articleManager.getArticles(withLang: "es")
        print("All articles in spanish returned:\n\(articles)\n")
        
        articles = articleManager.getArticles(containString: "remove")
        print("All articles with the world \"remove\" returned:\n\(articles)\n")
        
        let newArticle = articleManager.newArticle(title: "Article to remove", content: "this is the content to remove special world", image: nil, language: Language.FR.rawValue)
        print("One article created, \(newArticle)\n")
        
        articles = articleManager.getArticles(containString: "remove")
        print("All articles with the world \"remove\" returned:\n\(articles)\n")
        
        articles = articleManager.getArticles(containString: "special world")
        print("All articles with the world \"special world\" returned:\n\(articles)\n")
        
        articles = articleManager.getAllArticles()
        print("All articles returned:\n\(articles)\n")
        
        articles = articleManager.getArticles(withLang: "en")
        print("All articles in english returned:\n\(articles)")
        
        articleManager.removeArticle(article: newArticle)
        print("One article removed\n")
        
        articles = articleManager.getAllArticles()
        print("All articles returned:\n\(articles)\n")
        
        
//        _ = articleManager.newArticle(title: "Article in english", content: "article in english", image: nil, language: "en")
        
        articleManager.save()
        print("save OK\n")
        
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

