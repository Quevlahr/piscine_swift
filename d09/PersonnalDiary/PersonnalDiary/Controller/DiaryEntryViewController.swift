//
//  DiaryEntryViewController.swift
//  PersonnalDiary
//
//  Created by Quentin ROULON on 10/12/18.
//  Copyright © 2018 Quentin ROULON. All rights reserved.
//

import UIKit
import quroulon2018

class DiaryEntryViewController: UIViewController, UITextFieldDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    /***************
     *   VARIABLES
     ***************/
    
    var currentArticle: Article!
    
    @IBOutlet weak var titleLabel: UITextField! {
        didSet { titleLabel.delegate = self }
    }
    @IBOutlet weak var contentLabel: UITextView!
    @IBOutlet weak var diaryImageView: UIImageView!
    @IBOutlet weak var deleteButton: UIButton!
    @IBAction func deletePicture(_ sender: UIButton) {
        currentArticle.image = nil
        deleteButton.isEnabled = false
        diaryImageView.image = nil
    }
    @IBAction func saveButton(_ sender: Any) {
        if !(titleLabel.text!.isEmpty) && !(contentLabel.text.isEmpty) {
            currentArticle.title = titleLabel.text
            currentArticle.content = contentLabel.text
            performSegue(withIdentifier: "unwindSegueToDiary", sender: currentArticle)
        }
    }
    
    /***************
     *   PICTURES
     ***************/
    
    let pickerController = UIImagePickerController()
    
    @IBAction func showPickerLibrary() {
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
            pickerController.sourceType = .photoLibrary
            present(pickerController, animated: true, completion: nil)
        }
    }
            
    @IBAction func showPickerCamera() {
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            pickerController.sourceType = .camera
            present(pickerController, animated: true, completion: nil)
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
            diaryImageView.image = image
            currentArticle.image = UIImagePNGRepresentation(image) as NSData?
            deleteButton.isEnabled = true
        } else {
            print("something went wrong")
        }
        self.dismiss(animated: true, completion: nil)
    }
    
    /***************
     *   CLAVIER
     ***************/
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    /***************
     *  PROTOCOLS
     ***************/
    
    override func viewDidLoad() {
        super.viewDidLoad()

        titleLabel.text = currentArticle.title
        contentLabel.text = currentArticle.content
        if let image = currentArticle.image {
            diaryImageView.image = UIImage(data: image as Data)
        }

        contentLabel.layer.borderWidth = 1
        contentLabel.layer.borderColor = UIColor(red: 0.902, green: 0.902, blue: 0.902, alpha: 1).cgColor
        contentLabel.layer.cornerRadius = 5

        let tap = UITapGestureRecognizer(target: self.view, action: #selector(UIView.endEditing(_:)))
        tap.cancelsTouchesInView = false
        self.view.addGestureRecognizer(tap)
        
        if currentArticle.image != nil {
            deleteButton.isEnabled = true
        } else {
            deleteButton.isEnabled = false
        }
        
        self.pickerController.delegate = self
    }
}
