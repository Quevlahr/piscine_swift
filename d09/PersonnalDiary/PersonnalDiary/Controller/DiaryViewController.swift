//
//  DiaryViewController.swift
//  PersonnalDiary
//
//  Created by Quentin ROULON on 10/12/18.
//  Copyright © 2018 Quentin ROULON. All rights reserved.
//

import UIKit
import quroulon2018

class DiaryViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    /**************
    *   VARIABLES
    ***************/
    
    var articleManager: ArticleManager!
    var articleTmp: Article?
    var articles: [Article] = []
    var isUpdated: Bool = false
    

    @IBOutlet weak var navigationBar: UINavigationItem!
    @IBOutlet weak var diaryTableView: UITableView! {
        didSet {
            self.diaryTableView.delegate = self
            self.diaryTableView.dataSource = self
        }
    }
    
    @IBAction func addEntryButton(_ sender: UIBarButtonItem) {
        self.isUpdated = false
        performSegue(withIdentifier: "segueToAddEntryToDiary", sender: nil)
    }
    
    @IBAction func unwindSegueToDiary(segue: UIStoryboardSegue) {
        articles = articleManager.getArticles(withLang: Locale.current.languageCode ?? "en")
        if self.isUpdated {
            articleTmp?.updatedAt = NSDate()
            print("have bee isUpdated ? :", isUpdated)
        }
        diaryTableView.reloadData()
        diaryTableView.endUpdates()
        articleManager.save()
        articleTmp = nil
    }
    
    /***************
     *  PROTOCOLS
     ***************/

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return articles.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "diaryCell") as? DiaryTableViewCell
        cell?.displayCell(of: articles[indexPath.row])
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.articleTmp = articles[indexPath.row]
        self.isUpdated = true
        performSegue(withIdentifier: "segueToAddEntryToDiary", sender: nil)
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == UITableViewCellEditingStyle.delete {
            articleManager.removeArticle(article: articles[indexPath.row])
            articleManager.save()
            
            articles = articleManager.getArticles(withLang: Locale.current.languageCode ?? "en")
            self.diaryTableView.reloadData()
            self.diaryTableView.endUpdates()
//            self.articles.remove(at: indexPath.row)
//            self.diaryTableView.deleteRows(at: [indexPath], with: .fade)
        }
    }
    
    /***************
     *  OVERRIDE
     ***************/
    
    override func viewWillAppear(_ animated: Bool) {
        if let articleToRemove = articleTmp {
            if !self.isUpdated {
                articleManager.removeArticle(article: articleToRemove)
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        navigationBar.hidesBackButton = true

        print(String(describing: Locale.current.languageCode))
//        Optional("en")
        
        articleManager = ArticleManager()
        articles = articleManager.getArticles(withLang: Locale.current.languageCode ?? "en")
        
        articleTmp = nil
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "segueToAddEntryToDiary" {
            if let destination = segue.destination as? DiaryEntryViewController {
                if !isUpdated {
                    articleTmp = articleManager.newArticle()
                }
                destination.currentArticle = articleTmp
            }
        }
    }
}
