//
//  DiaryTableViewCell.swift
//  PersonnalDiary
//
//  Created by Quentin ROULON on 10/12/18.
//  Copyright © 2018 Quentin ROULON. All rights reserved.
//

import UIKit
import quroulon2018

class DiaryTableViewCell: UITableViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var diaryImageView: UIImageView!
    @IBOutlet weak var contentLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var updateDateLabel: UILabel!
    @IBOutlet weak var updateDateHeight: NSLayoutConstraint!
    @IBOutlet weak var imageViewHeight: NSLayoutConstraint!
    
    func displayCell(of article: Article) {
        titleLabel.text = article.title
        contentLabel.text = article.content
        
        let formatter = DateFormatter()
        formatter.dateFormat = "'Creation : 'EEEE, MMMM d, yyyy 'at' h:mm:ss a"
        dateLabel.text = formatter.string(from: article.createdAt as Date!)
        if article.createdAt != article.updatedAt {
            formatter.dateFormat = "'Update : 'EEEE, MMMM d, yyyy 'at' h:mm:ss a"
            updateDateLabel.isHidden = false
            updateDateHeight.constant = 16
            updateDateLabel.text = formatter.string(from: article.updatedAt as Date!)
        } else {
            updateDateHeight.constant = 0
            updateDateLabel.isHidden = true
        }
                
        if let image = article.image {
            diaryImageView.isHidden = false
//            diaryImageView.heightAnchor.constraint(lessThanOrEqualToConstant: 250)
            imageViewHeight.constant = 250
            diaryImageView.image = UIImage(data: image as Data)
        } else {
            diaryImageView.isHidden = true
            imageViewHeight.constant = 0
//            diaryImageView.heightAnchor.constraint(equalToConstant: 0)
        }
    }
}
