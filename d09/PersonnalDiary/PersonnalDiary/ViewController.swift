//
//  ViewController.swift
//  PersonnalDiary
//
//  Created by Quentin ROULON on 10/12/18.
//  Copyright © 2018 Quentin ROULON. All rights reserved.
//

import UIKit
import LocalAuthentication

class ViewController: UIViewController {
    
    let authenticationContext = LAContext()
    @IBOutlet weak var authenticationLabel: UILabel!
    
    func recursiveAuthentication() {
        if authenticationContext.canEvaluatePolicy(.deviceOwnerAuthentication, error: nil) {
            authenticationContext.evaluatePolicy(.deviceOwnerAuthentication, localizedReason: "You need to be authenticate") {
                (success, error) in
                if success {
                    
                    DispatchQueue.main.async {
                        self.authenticationLabel.text = NSLocalizedString("Successfull authentication", comment: "")
                        self.performSegue(withIdentifier: "segueToDiary", sender: nil)
                    }
                } else {
                    DispatchQueue.main.sync {
                        self.authenticationLabel.text = NSLocalizedString("Authentication failed", comment: "")
                    }
                    
                    print(error!)
                    self.recursiveAuthentication()
                }
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
     
        self.recursiveAuthentication()
    }
}

