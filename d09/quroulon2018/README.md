# quroulon2018

[![CI Status](https://img.shields.io/travis/Quevlahr/quroulon2018.svg?style=flat)](https://travis-ci.org/Quevlahr/quroulon2018)
[![Version](https://img.shields.io/cocoapods/v/quroulon2018.svg?style=flat)](https://cocoapods.org/pods/quroulon2018)
[![License](https://img.shields.io/cocoapods/l/quroulon2018.svg?style=flat)](https://cocoapods.org/pods/quroulon2018)
[![Platform](https://img.shields.io/cocoapods/p/quroulon2018.svg?style=flat)](https://cocoapods.org/pods/quroulon2018)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

quroulon2018 is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'quroulon2018'
```

## Author

Quevlahr, quroulon@student.42.fr

## License

quroulon2018 is available under the MIT license. See the LICENSE file for more info.
