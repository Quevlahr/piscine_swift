//
//  ArticleManager.swift
//  Pods-quroulon2018_Example
//
//  Created by Quentin ROULON on 10/11/18.
//

import Foundation
import CoreData

@available(iOS 10.0, *)
public class ArticleManager {
    
//    let persistentContainer: NSPersistentContainer
    var managedObjectContext: NSManagedObjectContext

    // Create a new article
//    public func newArticle(title: String, content: String, image: NSData?, language: String) -> Article {
    public func newArticle() -> Article {
        let article = Article(context: self.managedObjectContext)
        
//        article.title = title
//        article.content = content
        let date = NSDate()
        article.createdAt = date
        article.updatedAt = date
//        article.image = image
//        switch language {
//        case Language.FR.rawValue, Language.EN.rawValue:
//            article.language = language
//        default:
//            article.language = Language.FR.rawValue
//        }
        article.language = Locale.current.languageCode
        
        return article
    }
    
    // return all articles in context
    public func getAllArticles() -> [Article] {
        let request = self.getRequest()
        
        do {
            let result = try self.managedObjectContext.fetch(request)
            guard let articles = result as? [Article] else {
                fatalError("Can't convert [Any] in [Article]\n")
            }
            return articles
        } catch {
            fatalError("Can't getAllArticles\n")
        }
    }
    
    // return all articles for a language
    public func getArticles(withLang lang: String) -> [Article] {
        let request = self.getRequest()
        request.predicate = NSPredicate(format: "language == %@", lang)
        
        do {
            let result = try self.managedObjectContext.fetch(request)
            guard let articles = result as? [Article] else {
                fatalError("Can't convert [Any] in [Article]\n")
            }
            return articles
        } catch {
            fatalError("Can't getAllArticles\n")
        }
    }
    
    // return all articles which contains the given string
    public func getArticles(containString str: String) -> [Article] {
        let request = self.getRequest()
        request.predicate = NSPredicate(format: "title contains[c] %@ OR content contains[c] %@", argumentArray: [str, str])
        
        do {
            let result = try self.managedObjectContext.fetch(request)
            guard let articles = result as? [Article] else {
                fatalError("Can't convert [Any] in [Article]\n")
            }
            return articles
        } catch {
            fatalError("Can't getAllArticles\n")
        }
    }
    
    // remove the given article
    public func removeArticle(article: Article) {
        self.managedObjectContext.delete(article)
    }
    
    // save the new articles or deleting one
    public func save() {
        do {
//            for object in self.managedObjectContext.updatedObjects {
//                if let article = object as? Article {
//                    article.updatedAt = NSDate()
//                }
//            }
            try self.managedObjectContext.save()
        } catch {
            print("Failed saving\n")
        }
    }
    
    func getRequest() -> NSFetchRequest<NSFetchRequestResult> {
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Article")
        request.returnsObjectsAsFaults = false
        return request
    }
    
    public init() {
        
        // GET persistentContainer
//        self.persistentContainer = NSPersistentContainer(name: "Article")
//        self.persistentContainer.loadPersistentStores(completionHandler: { (storeDescription, error) in
//            if let error = error {
//                fatalError("Failed to load Core Data stack, \((error as NSError).userInfo)")
//            }
//        })

        let bundle = Bundle(for: Article.self)
        guard let modelURL = bundle.url(forResource: "Article", withExtension: "momd") else {
            fatalError("Error loading model from bundle")
        }
        guard let mom = NSManagedObjectModel(contentsOf: modelURL) else {
            fatalError("Error initializing mom from: \(modelURL)")
        }
        
        let psc = NSPersistentStoreCoordinator(managedObjectModel: mom)
    
        // GET managedObjectContext
        self.managedObjectContext = NSManagedObjectContext(concurrencyType: NSManagedObjectContextConcurrencyType.mainQueueConcurrencyType)
        self.managedObjectContext.persistentStoreCoordinator = psc
        self.managedObjectContext.performAndWait {
            
            guard let docURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).last else {
                fatalError("Unable to resolve document directory")
            }
            
            let storeURL = docURL.appendingPathComponent("Article.sqlite")
            do {
                try psc.addPersistentStore(ofType: NSSQLiteStoreType, configurationName: nil, at: storeURL, options: nil)
            } catch {
                fatalError("Error migrating store: \(error)")
            }
        }
    }
}


