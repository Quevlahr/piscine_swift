//
//  Language.swift
//  Pods
//
//  Created by Quentin ROULON on 10/11/18.
//

import Foundation

public enum Language: String {
    case FR = "fr"
    case EN = "en"
}

