//
//  Article+CoreDataProperties.swift
//  Pods
//
//  Created by Quentin ROULON on 10/11/18.
//
//

import Foundation
import CoreData


extension Article {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Article> {
        return NSFetchRequest<Article>(entityName: "Article")
    }

    @NSManaged public var title: String?
    @NSManaged public var content: String?
    @NSManaged public var language: String?
    @NSManaged public var image: NSData?
    @NSManaged public var createdAt: NSDate?
    @NSManaged public var updatedAt: NSDate?

    override public var description: String {
        return "\(title ?? "") created at \(createdAt ?? NSDate()) in \(language ?? "fr"): \n\(content ?? "")\n"
    }
}
