#
# Be sure to run `pod lib lint quroulon2018.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'quroulon2018'
  s.version          = '0.1.0'
  s.summary          = 'To create Article'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
creation of articles
                       DESC

  s.homepage         = 'https://github.com/Quevlahr/quroulon2018'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'Quevlahr' => 'quroulon@student.42.fr' }
  s.source           = { :git => 'https://github.com/Quevlahr/quroulon2018.git', :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.ios.deployment_target = '8.0'

  s.source_files = 'quroulon2018/Classes/*.swift'
  s.resource = 'quroulon2018/Classes/*.xcdatamodeld'
  
  
  # s.resource_bundles = {
  #   'quroulon2018' => ['quroulon2018/Assets/*.png']
  # }

  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
  # s.dependency 'AFNetworking', '~> 2.3'
end
