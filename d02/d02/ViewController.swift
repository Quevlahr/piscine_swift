//
//  ViewController.swift
//  d02
//
//  Created by Quentin ROULON on 10/2/18.
//  Copyright © 2018 Quentin ROULON. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var deathNoteTableView: UITableView! {
        didSet {
            deathNoteTableView.delegate = self
            deathNoteTableView.dataSource = self
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Data.death.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "deathNoteCell") as? DeathNoteTableViewCell
        cell?.informations = Data.death[indexPath.row]
        return cell!
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "addDeathSegue") {
        }
    }
    
    @IBAction func unwindToViewController(segue: UIStoryboardSegue) {
        deathNoteTableView.reloadData()
        deathNoteTableView.endUpdates()
    }
}

