//
//  Model.swift
//  d02
//
//  Created by Quentin ROULON on 10/2/18.
//  Copyright © 2018 Quentin ROULON. All rights reserved.
//

import Foundation

struct Data	{
    static var death : [(name: String, description: String, date: Date)] = [
        ("Quentin Roulon", "Mort en glissant sur une banane", Date()),
        ("Jean Dupont", "Mort comme ca sans faire expres", Date()),
        ("Olivier Yop", "Suicide", Date()),
        ("Trop long mais pas trop bon c'est juste beaucoup trop grand et moche", "The width of both labels should extend to fill the width of the cell’s content view, and the headline label should appear above the body label. To accomplish this, Auto Layout constraints are added to each label starting with constraints that define the label width. For the headline label, constraints are added that tell the label to fill the space between the content view’s leading and trailing margins. For the body label, constraints are added that set its leading and trailing anchors equal to the headline label’s \nleading\n and\n trailing\n anchors.", Date())
    ]
}
