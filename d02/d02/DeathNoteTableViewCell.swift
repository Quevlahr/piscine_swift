//
//  DeathNoteTableViewCell.swift
//  d02
//
//  Created by Quentin ROULON on 10/2/18.
//  Copyright © 2018 Quentin ROULON. All rights reserved.
//

import UIKit

class DeathNoteTableViewCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    
    var informations: (name: String, description: String, date: Date)? {
        didSet {
            if let info = informations {
                nameLabel?.text = info.name
                descriptionLabel?.text = info.description
                                
                let formatter = DateFormatter()
                formatter.dateFormat = "dd-MM-yyyy"
                
                let myDate = formatter.string(from: info.date)
                dateLabel?.text = myDate

                
            }
        }
    }
}
