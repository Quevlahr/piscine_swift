//
//  AddDeathViewController.swift
//  d02
//
//  Created by Quentin ROULON on 10/3/18.
//  Copyright © 2018 Quentin ROULON. All rights reserved.
//

import UIKit

class AddDeathViewController: UIViewController {
    
    override func viewDidLoad() {
        descriptionOfDeathText.layer.borderWidth = 0.5
        descriptionOfDeathText.layer.borderColor = UIColor.darkGray.cgColor
        descriptionOfDeathText.layer.cornerRadius = 8
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "unwindToViewController") {
        }
    }

    @IBAction func addAPerson(_ sender: UIBarButtonItem) {
        print("\nName : \n" + namePersonText.text!, "\n\nDescription : \n" + descriptionOfDeathText.text, "\n\nDate : \n", dateOfDeathPicker.date)
        if (namePersonText.text! != "") {
            Data.death.append((namePersonText.text!, descriptionOfDeathText.text, dateOfDeathPicker.date))
        }
        performSegue(withIdentifier: "unwindToViewController", sender: (
            name: namePersonText.text,
            description: descriptionOfDeathText.text,
            date: dateOfDeathPicker.date)
        )
    }
    
    @IBOutlet weak var dateOfDeathPicker: UIDatePicker! {
        didSet {
            dateOfDeathPicker.minimumDate = Date()
        }
    }
    @IBOutlet weak var namePersonText: UITextField!
    @IBOutlet weak var descriptionOfDeathText: UITextView!

}
