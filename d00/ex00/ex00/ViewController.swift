//
//  ViewController.swift
//  ex00
//
//  Created by Quentin ROULON on 10/1/18.
//  Copyright © 2018 Quentin ROULON. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var displayResultLabel: UILabel!
    var leftPart = 0;
    var tempValue = 0;
    var sign = ""
    var hasClicked = false
    let additionSign = "+", substractionSign = "-", multiplicationSign = "*", divisionSign = "/"

    func calculResult() {
        if (sign == additionSign) {
            let report = leftPart.addingReportingOverflow(tempValue)
            if (report.overflow == false) {
                leftPart = report.partialValue
            }
        } else if (sign == substractionSign) {
            let report = tempValue.subtractingReportingOverflow(leftPart)
            if (report.overflow == false) {
                leftPart = report.partialValue
            }
        } else if (sign == multiplicationSign) {
            let report = leftPart.multipliedReportingOverflow(by: tempValue)
            if (report.overflow == false) {
                leftPart = report.partialValue
            }
        } else if (leftPart != 0 && sign == divisionSign) {
            let report = tempValue.dividedReportingOverflow(by: leftPart)
            if (report.overflow == false) {
                leftPart = report.partialValue
            }
        }
        tempValue = leftPart
        displayResultLabel.text = String(leftPart)
    }
    
    func addTouchClicked(toAdd: Int) {
        if (hasClicked == true) {
            leftPart = 0
            hasClicked = false
        }
        
        var report = leftPart.multipliedReportingOverflow(by: 10)
        if (report.overflow == false) {
            if (report.partialValue < 0) {
                report = report.partialValue.subtractingReportingOverflow(toAdd)
            } else {
                report = report.partialValue.addingReportingOverflow(toAdd)
            }
            if (report.overflow == false) {
                leftPart = report.partialValue
                displayResultLabel.text = String(leftPart)
                print("leftPart :", leftPart)
            } else {
                print("max of Int is ", Int.max)
            }
        } else {
            print("max of Int is ", Int.max)
        }
    }
    
    func addSignClicked(toUse: String) {
        if (hasClicked == true) {
            return
        }
        hasClicked = true
        calculResult()
        sign = toUse
    }
    
    
    @IBAction func cancelButton(_ sender: UIButton) {
        print("Cancel button")
        leftPart = 0
        tempValue = 0
        sign = ""
        displayResultLabel.text = "0"
    }
    @IBAction func deleteButton(_ sender: UIButton) {
        print("Delete button")
        if (leftPart > 9) {
            leftPart /= 10
        } else if (leftPart < -9) {
            leftPart /= 10
        } else {
            leftPart = 0
        }
        displayResultLabel.text = String(leftPart)
    }
    @IBAction func additionButton(_ sender: UIButton) {
        print("Addition button")
        addSignClicked(toUse: additionSign)
    }
    @IBAction func substractionButton(_ sender: UIButton) {
        print("Substraction button")
        addSignClicked(toUse: substractionSign)
    }
    @IBAction func multiplicationButton(_ sender: UIButton) {
        print("Multiplication button")
        addSignClicked(toUse: multiplicationSign)
    }
    @IBAction func divisionButton(_ sender: UIButton) {
        print("Division button")
        addSignClicked(toUse: divisionSign)
    }
    @IBAction func negativeButton(_ sender: UIButton) {
        print("Negative button")
        if (hasClicked == false) {
            let report = leftPart.multipliedReportingOverflow(by: -1)
            if (report.overflow == false) {
                leftPart = report.partialValue
                displayResultLabel.text = String(leftPart)
            }
        }
    }
    @IBAction func equalButton(_ sender: UIButton) {
        print("Equal button")
        if (hasClicked == true) {
            return
        }
        calculResult()
        sign = ""
    }
    
    
    @IBAction func zeroButton(_ sender: UIButton) {
        addTouchClicked(toAdd: 0)
    }
    @IBAction func oneButton(_ sender: UIButton) {
        addTouchClicked(toAdd: 1)
    }
    @IBAction func twoButton(_ sender: UIButton) {
        addTouchClicked(toAdd: 2)
    }
    @IBAction func threeButton(_ sender: UIButton) {
        addTouchClicked(toAdd: 3)
    }
    @IBAction func fourButton(_ sender: UIButton) {
        addTouchClicked(toAdd: 4)
    }
    @IBAction func fiveButton(_ sender: UIButton) {
        addTouchClicked(toAdd: 5)
    }
    @IBAction func sixButton(_ sender: UIButton) {
        addTouchClicked(toAdd: 6)
    }
    @IBAction func sevenButton(_ sender: UIButton) {
        addTouchClicked(toAdd: 7)
    }
    @IBAction func eightButton(_ sender: UIButton) {
        addTouchClicked(toAdd: 8)
    }
    @IBAction func nineButton(_ sender: UIButton) {
        addTouchClicked(toAdd: 9)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

