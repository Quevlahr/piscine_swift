import Foundation

class Deck : NSObject {
    static let allSpades: [Card] = Value.allValues.map {Card(color: Color.Spade, value: $0)}
    static let allDiamonds: [Card] = Value.allValues.map {Card(color: Color.Diamond, value: $0)}
    static let allHearts: [Card] = Value.allValues.map {Card(color: Color.Heart, value: $0)}
    static let allClubs: [Card] = Value.allValues.map {Card(color: Color.Club, value: $0)}
    static let allCards: [Card] = allDiamonds + allSpades + allHearts + allClubs
    
    var cards: [Card] = allCards
    var discards: [Card] = []
    var outs: [Card] = []
    
    init(isSort: Bool) {
        if (isSort == false) {
            cards.shuffle()
        }
    }
    
    override var description: String {
        var str = ""
        for card in cards {
            str += card.description + "\n"
        }
        return str
    }
    
    func describe() -> String {
        var str = ""
        str += "Cards in the deck : \n"
        for card in cards {
            str += card.description + "\n"
        }
        str += "Cards in the out : \n"
        for card in outs {
            str += card.description + "\n"
        }
        str += "Cards in the discard \n"
        for card in discards {
            str += card.description + "\n"
        }
        return str
    }
    
    func draw() -> Card? {
        if (cards.isEmpty == false) {
            outs.append(cards.removeFirst())
            return outs.last
        }
        return nil
    }
    
    func fold(c: Card) {
        if let index = outs.index(of: c) {
            discards.append(outs.remove(at: index))
        }
    }
}

extension Array {
    mutating func shuffle() {
        let uint32_count = UInt32(count)
        for i in 0 ..< count {
            let tmp = self[i]
            let dest = Int(arc4random_uniform(uint32_count))
            if (i != dest) {
                self[i] = self[dest]
                self[dest] = tmp
            }
        }
    }
}