// var firstCard: Card = Card(color: Color.Diamond, value: Value.King)
// var secondCard: Card = Card(color: Color.Club, value: Value.Five)
// var thirdCard: Card = Card(color: Color.Club, value: Value.Five)
// var fourthCard: Card = Card(color: Color.Club, value: Value.King)

// print(firstCard)

// print(firstCard, "==", secondCard, "->", firstCard.isEqual(secondCard))
// print(secondCard, "==", thirdCard, "->", secondCard.isEqual(thirdCard))
// print(thirdCard, "==", secondCard, "->", thirdCard.isEqual(secondCard))
// print(fourthCard, "==", firstCard, "->", fourthCard.isEqual(firstCard))

// print(firstCard, "== nil", "->", firstCard.isEqual(nil))
// print(firstCard, "==", Color.Club, "->", firstCard.isEqual(Color.Club))

// print(firstCard, "==", secondCard, "->", firstCard == secondCard)
// print(secondCard, "==", thirdCard, "->", secondCard == thirdCard)
// print(thirdCard, "==", secondCard, "->", thirdCard == secondCard)
// print(fourthCard, "==", firstCard, "->", fourthCard == firstCard)

// for card in Deck.allCards {
//     print(card)
// }

// for card in Deck.allClubs {
//     print(card)
// }

// for card in Deck.allHearts {
//     print(card)
// }

// for card in Deck.allSpades {
//     print(card)
// }

// for card in Deck.allDiamonds {
//     print(card)
// }

// var cards: [Card] = Deck.allCards
// cards.shuffle()
// for card in cards {
//     print(card)
// }


var deck: Deck = Deck(isSort: false)
deck.fold(c: deck.cards[1])
for _ in 0 ..< 55 {
    if let drawed = deck.draw() {
        print("Draw", drawed)
    } else {
        print("No more card")
    }
}
print(deck, "\n\n\n")
for out in deck.outs {
    deck.fold(c: out)
}


var deckSort: Deck = Deck(isSort: true)
for _ in 0 ..< 1 {
    if let drawed = deckSort.draw() {
        print("First card of the deck", drawed)
    } else {
        print("No more card")
    }
}
print(deckSort.describe(), "\n\n\n")



var deck3 = Deck(isSort: false)
for _ in 0 ..< 20 {
    _ = deck3.draw()
}
for i in 0 ..< 5 {
    deck3.fold(c: deck3.outs[i])
}
print(deck3.describe(), "\n\n\n")
