import Foundation

class Card : NSObject {
    var color: Color
    var value: Value
    
    init(color: Color, value: Value) {
        self.value = value
        self.color = color
    }
    
    override var description: String {
        return "(\(value.rawValue), \(color))"
    }
    
    override func isEqual(_ object: Any?) -> Bool {
        let card = object as? Card
        if (card?.value == self.value && card?.color == self.color) {
            return true
        }
        return false
    }
    
    static func ==(left: Card, right: Card) -> Bool {
        if (left.value == right.value && left.color == right.color) {
            return true
        }
        return false
    }
}