var oneCard: (color: Color, value: Value) = (Color.Spade, Value.Eight)
print(oneCard.color, oneCard.value.rawValue)

for color in Color.allColors {
	print(color, ":", color.rawValue + ", hashValue ->", color.hashValue)
}

for value in Value.allValues {
	print(value, ":", String(value.rawValue) + ", hashValue ->", value.hashValue)
}

print(Color.allColors)
print(Value.allValues)