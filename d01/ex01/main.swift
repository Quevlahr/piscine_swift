var firstCard: Card = Card(color: Color.Diamond, value: Value.King)
var secondCard: Card = Card(color: Color.Club, value: Value.Five)
var thirdCard: Card = Card(color: Color.Club, value: Value.Five)
var fourthCard: Card = Card(color: Color.Club, value: Value.King)

print(firstCard)

print(firstCard, "==", secondCard, "->", firstCard.isEqual(secondCard))
print(secondCard, "==", thirdCard, "->", secondCard.isEqual(thirdCard))
print(thirdCard, "==", secondCard, "->", thirdCard.isEqual(secondCard))
print(fourthCard, "==", firstCard, "->", fourthCard.isEqual(firstCard))

print(firstCard, "== nil", "->", firstCard.isEqual(nil))
print(firstCard, "==", Color.Club, "->", firstCard.isEqual(Color.Club))

// var optionnalValue: Int?

print(firstCard, "==", secondCard, "->", firstCard == secondCard)
print(secondCard, "==", thirdCard, "->", secondCard == thirdCard)
print(thirdCard, "==", secondCard, "->", thirdCard == secondCard)
print(fourthCard, "==", firstCard, "->", fourthCard == firstCard)
// print(firstCard, "==", optionnalValue, "->", firstCard == optionnalValue)
// print(firstCard, "==", Color.Club, "->", firstCard == Color.Club)

