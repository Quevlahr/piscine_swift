////: Playground - noun: a place where people can play
//
import UIKit
//
//enum Color : String {
//    case Club = "trèfle"
//    case Diamond = "carreau"
//    case Heart = "coeur"
//    case Spade = "pique"
//    static let allColors: [Color] = [.Club, .Diamond, .Heart, .Spade]
//}
//
////for color in Color.allColors {
////    print(color, color.hashValue, color.rawValue)
////}
////print(Color.allColors)
//
//enum Value : Int {
//    case Ace = 1
//    case Two = 2
//    case Three = 3
//    case Four = 4
//    case Five = 5
//    case Six = 6
//    case Seven = 7
//    case Eight = 8
//    case Nine = 9
//    case Ten = 10
//    case Jack = 11
//    case Queen = 12
//    case King = 13
//    static let allValues: [Value] = [.Ace, .Two, .Three, .Four, .Five, .Six, .Seven, .Eight, .Nine, .Ten, .Jack, .Queen, .King]
//}
//
////print(Value.allValues)
//
//class Card : NSObject {
//    var color: Color
//    var value: Value
//
//    init(color: Color, value: Value) {
//        self.value = value
//        self.color = color
//    }
//
//    override var description: String {
//        return "(\(value.rawValue), \(color))"
//    }
//
//    override func isEqual(_ object: Any?) -> Bool {
//        if let card = object as? Card {
//            if (card.value == self.value && card.color == self.color) {
//                return true
//            }
//        }
//        return false
////        let card = object as? Card
////        if (card?.value == self.value && card?.color == self.color) {
////            return true
////        }
////        return false
//    }
//
//    static func ==(left: Card, right: Card) -> Bool {
//        if (left.value == right.value && left.color == right.color) {
//            return true
//        }
//        return false
//    }
//}
//
//
//var firstCard: Card = Card(color: Color.Diamond, value: Value.King)
//var secondCard: Card = Card(color: Color.Club, value: Value.Five)
//var thirdCard: Card = Card(color: Color.Club, value: Value.Five)
//var fourthCard: Card = Card(color: Color.Club, value: Value.King)
//
//
//class Deck : NSObject {
//    static let allSpades: [Card] = Value.allValues.map {Card(color: Color.Spade, value: $0)}
//    static let allDiamonds: [Card] = Value.allValues.map {Card(color: Color.Diamond, value: $0)}
//    static let allHearts: [Card] = Value.allValues.map {Card(color: Color.Heart, value: $0)}
//    static let allClubs: [Card] = Value.allValues.map {Card(color: Color.Club, value: $0)}
//    static let allCards: [Card] = allDiamonds + allSpades + allHearts + allClubs
//
//    var cards: [Card] = allCards
//    var discards: [Card] = []
//    var outs: [Card] = []
//
//    init(isSort: Bool) {
//        if (isSort == false) {
//            cards.shuffle()
//        }
//    }
//
//    override var description: String {
//        var str = ""
//        for card in cards {
//            str += card.description + "\n"
//        }
//        return str
//    }
//
//    func describe() -> String {
//        var str = ""
//        str += "Cards in the deck : \n"
//        for card in cards {
//            str += card.description + "\n"
//        }
//        str += "Cards in the out : \n"
//        for card in outs {
//            str += card.description + "\n"
//        }
//        str += "Cards in the discard \n"
//        for card in discards {
//            str += card.description + "\n"
//        }
//        return str
//    }
//
//    func draw() -> Card? {
//        if (cards.isEmpty == false) {
//            outs.append(cards.removeFirst())
//            return outs.last
//        }
//        return nil
//    }
//
//    func fold(c: Card) {
//        if let index = outs.index(of: c) {
//            discards.append(outs.remove(at: index))
//        }
//    }
//}
//
//extension Array {
//    mutating func shuffle() {
//        let uint32_count = UInt32(count)
//        for i in 0 ..< count {
//            let tmp = self[i]
//            let dest = Int(arc4random_uniform(uint32_count))
//            if (i != dest) {
//                self[i] = self[dest]
//                self[dest] = tmp
//            }
//        }
//    }
//}
//
//
//var deck: Deck = Deck(isSort: false)
//deck.fold(c: deck.cards[1])
//for _ in 0 ..< 55 {
//    if let drawed = deck.draw() {
//        print("Draw", drawed)
//    } else {
//        print("No more card")
//    }
//}
//print(deck, "\n\n\n")
//for out in deck.outs {
//    deck.fold(c: out)
//}
//
//
//var deckSort: Deck = Deck(isSort: true)
//for _ in 0 ..< 1 {
//    if let drawed = deckSort.draw() {
//        print("First card of the deck", drawed)
//    } else {
//        print("No more card")
//    }
//}
//print(deckSort.describe(), "\n\n\n")
//
//
//
//var deck3 = Deck(isSort: false)
//for _ in 0 ..< 20 {
//    deck3.draw()
//}
//for i in 0 ..< 5 {
//    deck3.fold(c: deck3.outs[i])
//}
//print(deck3.describe(), "\n\n\n")
//














import XCPlayground

//let CUSTOMER_KEY = "zlqU9bJW7vurMcaGhLRY2lBCD"
//let CUSTOMER_KEY = "bNrW9SPnwLvijAeQQYdwroyzB"
let CUSTOMER_KEY = "eDOr2OcMcWm6FkRVCVaJn7uon"
//let CUSTOMER_SECRET = "TLYTAJFnyczV9SqSWEORX2iXI8zaG0xpN4GSbeXnevJgUeJKbc"
//let CUSTOMER_SECRET = "XxU7Z2RNIJyQjiQYWHxQGQT4bth3QBXoKfI1kuqEzCOThwczrl"
let CUSTOMER_SECRET = "Zr2unsYSDqs6fyfARpl4dTpr2EEdZH5bnoxXEoWFzXjukQu0Yn"

let BEARER = ((CUSTOMER_KEY + ":" + CUSTOMER_SECRET).data(using: String.Encoding.utf8))!.base64EncodedString(options: Data.Base64EncodingOptions(rawValue:0))
//print(BEARER)
let url = URL(string: "https://api.twitter.com/oauth2/token")
let request = NSMutableURLRequest(url: url!)
request.httpMethod = "POST"
request.setValue("Basic " + BEARER, forHTTPHeaderField: "Authorization")
request.setValue("application/x-www-form-urlencoded;charset=UTF-8", forHTTPHeaderField: "Content-Type")
request.httpBody = "grant_type=client_credentials".data(using: String.Encoding.utf8)

//let dictionary = ["access_token" : "AAAAAAAAAAAAAAAAAAAAAPV08gAAAAAA0hywGN9mAViYhuX6C8EcjJw3ZqM%3DLd7pc3FBawOIKHQrz5nI1307b1Od9SUIe0FUfZsJvZQtjKHOko",
//                  "token_type" : "bearer"]
//if let index = dictionary.index(forKey: "access_token") {
//    print(dictionary[index].value)
//    print(dictionary[index].key)
//}


var dictionary: String = ""
var dictionary2: String = ""

//print(request)
let task = URLSession.shared.dataTask(with: request as URLRequest) {
    (data, response, error) in
//    print(response!)
    if error != nil {
        print(error!)
    }
    else if let d = data {
        do {
            if let dic: NSDictionary = try JSONSerialization.jsonObject(with: d, options: JSONSerialization.ReadingOptions.mutableLeaves) as? NSDictionary {
                print(dic)
                if let access_token = dic["access_token"] as! String? {
                    dictionary = access_token
                }
                dictionary2 = dic["access_token"] as! String
            }
        }
        catch(let err) {
            print(err)
        }
    }
}
//task.resume()
//
//print(dictionary)
//print(dictionary2)
//XCPlaygroundPage.currentPage.needsIndefiniteExecution = true






















