enum Color : String {
    case Club = "trèfle"
    case Diamond = "carreau"
    case Heart = "coeur"
    case Spade = "pique"
    static let allColors: [Color] = [.Club, .Diamond, .Heart, .Spade]
}