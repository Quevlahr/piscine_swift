import Foundation

class Deck : NSObject {
    static let allSpades: [Card] = Value.allValues.map {Card(color: Color.Spade, value: $0)}
    static let allDiamonds: [Card] = Value.allValues.map {Card(color: Color.Diamond, value: $0)}
    static let allHearts: [Card] = Value.allValues.map {Card(color: Color.Heart, value: $0)}
    static let allClubs: [Card] = Value.allValues.map {Card(color: Color.Club, value: $0)}
    static let allCards: [Card] = allDiamonds + allSpades + allHearts + allClubs
}

extension Array {
    mutating func shuffle() {
        for i in 0 ..< count {
            let tmp = self[i]
            let dest = Int(arc4random_uniform(UInt32(count)))
            if (i != dest) {
                self[i] = self[dest]
                self[dest] = tmp
            }
        }
    }
}