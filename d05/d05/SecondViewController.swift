//
//  SecondViewController.swift
//  d05
//
//  Created by Quentin ROULON on 10/8/18.
//  Copyright © 2018 Quentin ROULON. All rights reserved.
//

import UIKit
import CoreLocation

class SecondViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    /************
     * VARIABLES
     ************/
    
    @IBOutlet weak var placesTableView: UITableView! {
        didSet {
            self.placesTableView.delegate = self
            self.placesTableView.dataSource = self
        }
    }
    
    /***********
    * PROTOCOLS
    ************/

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let coordinate = CLLocationCoordinate2D(latitude: DataPlace.places[indexPath.row].lat, longitude: DataPlace.places[indexPath.row].long)
        DataPlace.mapRegion.center = coordinate
        if let firstViewController = self.tabBarController?.viewControllers![0] as? FirstViewController {
            firstViewController.mapView.setRegion(DataPlace.mapRegion, animated: true)
            self.tabBarController?.selectedIndex = 0
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return DataPlace.places.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "placeCell") as! PlaceTableViewCell
        cell.displayCell(for: DataPlace.places[indexPath.row])
        return cell
    }
    
    /************
     * OVERRIDES
     ************/
}

