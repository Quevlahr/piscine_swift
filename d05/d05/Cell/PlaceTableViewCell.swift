//
//  PlaceTableViewCell.swift
//  d05
//
//  Created by Quentin ROULON on 10/8/18.
//  Copyright © 2018 Quentin ROULON. All rights reserved.
//

import UIKit

class PlaceTableViewCell: UITableViewCell {

    @IBOutlet weak var placeLabel: UILabel!
    
    func displayCell(for place: Place) {
        placeLabel.text = place.name
    }
    
//    override func awakeFromNib() {
//        super.awakeFromNib()
//        // Initialization code
//    }
//
//    override func setSelected(_ selected: Bool, animated: Bool) {
//        super.setSelected(selected, animated: animated)
//
//        // Configure the view for the selected state
//    }

}
