//
//  Place.swift
//  d05
//
//  Created by Quentin ROULON on 10/8/18.
//  Copyright © 2018 Quentin ROULON. All rights reserved.
//

import UIKit
import Foundation

struct Place {
    let name: String
    let subtitle: String
    let lat: Double
    let long: Double
    let color: UIColor
}
