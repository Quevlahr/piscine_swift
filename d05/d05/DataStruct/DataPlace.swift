//
//  DataPlace.swift
//  d05
//
//  Created by Quentin ROULON on 10/8/18.
//  Copyright © 2018 Quentin ROULON. All rights reserved.
//

import Foundation
import MapKit
import CoreLocation

struct DataPlace {
    static var locationManager: CLLocationManager = CLLocationManager()
    static var mapRegion: MKCoordinateRegion = MKCoordinateRegion()
    
    static var places: [Place] = [Place(name: "42", subtitle: "Ecole stylée", lat: 48.896662, long: 2.31833,
                                        color: UIColor(red: 0.2, green: 0.3, blue: 1.0, alpha: 1)),
                                  Place(name: "Brest", subtitle: "Perret for ever", lat: 48.390394, long: -4.486076,
                                        color: UIColor(red: 0.6, green: 0.2, blue: 0.35, alpha: 1)),
                                  Place(name: "Nantes", subtitle: "C'est pas en Bretagne", lat: 47.218371, long: -1.553621,
                                        color: UIColor(red: 0.25, green: 1, blue: 0.4, alpha: 1)),
                                  Place(name: "Le Havre", subtitle: "...", lat: 49.49437, long: 0.107929,
                                        color: UIColor(red: 1, green: 0.53, blue: 0.3, alpha: 1)),
                                  Place(name: "Montréal", subtitle: "Trop cool", lat: 45.501689, long: -73.567256,
                                        color: UIColor(red: 0.8, green: 0.7, blue: 1.0, alpha: 1)),
                                  Place(name: "Sidney", subtitle: "Parait que c'est la capitale", lat: -33.867487, long: 151.206990,
                                        color: UIColor(red: 0.7, green: 0.7, blue: 0.7, alpha: 1)),
                                  Place(name: "Canberra", subtitle: "Petite ville paumée", lat: -35.280937, long: 149.130009,
                                        color: UIColor(red: 0.2, green: 0.9, blue: 0.1, alpha: 1)),
                                  Place(name: "Antananarivo", subtitle: "loing", lat: -18.87919, long: 47.507905,
                                        color: UIColor(red: 0.9, green: 0.3, blue: 0.85, alpha: 1)),
                                  Place(name: "Oulan-Bator", subtitle: "loing et perdu", lat: 47.886399, long: 106.905744,
                                        color: UIColor(red: 0.6, green: 0.3, blue: 0.2, alpha: 1)),
                                  Place(name: "Fort-de-France", subtitle: "Soleil", lat: 14.616065, long: -61.05878,
                                        color: UIColor(red: 0.2, green: 0.7, blue: 0.5, alpha: 1))]
}
