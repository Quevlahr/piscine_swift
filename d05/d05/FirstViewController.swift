//
//  FirstViewController.swift
//  d05
//
//  Created by Quentin ROULON on 10/8/18.
//  Copyright © 2018 Quentin ROULON. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

class PointAnnotation42: MKPointAnnotation {
    var pinTintColor: UIColor?
}

class FirstViewController: UIViewController, MKMapViewDelegate, CLLocationManagerDelegate {

    /************
     * VARIABLES
     ************/
    
    @IBOutlet weak var mapView: MKMapView! {
        didSet {
            self.mapView.delegate = self
        }
    }
    @IBOutlet weak var mapStyleButtonOutlet: UISegmentedControl! {
        didSet {
            mapStyleButtonOutlet.layer.cornerRadius = 5
        }
    }
    
    /************
     * ACTIONS
     ************/
    
    @IBAction func centerToLocationButton(_ sender: UIButton) {
        if let currentLocation = DataPlace.locationManager.location {
            DataPlace.mapRegion.center = currentLocation.coordinate
            mapView.setRegion(DataPlace.mapRegion, animated: true)
        }
    }
    
    @IBAction func indexChangeSegmentedButton(_ sender: UISegmentedControl) {
        switch mapStyleButtonOutlet.selectedSegmentIndex {
        case 0:
            self.mapView.mapType = .standard;
        case 1:
            self.mapView.mapType = .satellite;
        default:
            self.mapView.mapType = .hybrid
        }
    }
    
    @IBAction func mapStyleButton(_ sender: UISegmentedControl) {
        
    }
    
    /************
     * PROTOCOLS
     ************/

    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {

        if annotation is MKUserLocation { return nil }
        let annotationView = MKPinAnnotationView(annotation: annotation, reuseIdentifier: "pin")

        annotationView.canShowCallout = true
        annotationView.animatesDrop = true
        annotationView.pinTintColor = UIColor.darkGray
        annotationView.isDraggable = true

        let btn = UIButton(type: .detailDisclosure)
        annotationView.rightCalloutAccessoryView = btn
        
        
        if let place = DataPlace.places.first(where: { ($0.name == annotation.title!) }) {
            annotationView.pinTintColor = place.color
        }

        return annotationView
    }

    /************
     * OVERRIDES
     ************/
    
    override func viewDidLoad() {
        super.viewDidLoad()

        DataPlace.locationManager.requestWhenInUseAuthorization()
        DataPlace.locationManager.delegate = self
        DataPlace.locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
        DataPlace.locationManager.distanceFilter = 10
        DataPlace.locationManager.startUpdatingLocation()
        
        let coordinate = CLLocationCoordinate2D(latitude: 48.896662, longitude: 2.31833)
        
        DataPlace.mapRegion.center = coordinate
        DataPlace.mapRegion.span.latitudeDelta = 0.01;
        DataPlace.mapRegion.span.longitudeDelta = 0.01;
        
        mapView.setRegion(DataPlace.mapRegion, animated: true)
        mapView.showsUserLocation = true
        
        for place in DataPlace.places {
            let coordinate = CLLocationCoordinate2D(latitude: place.lat, longitude: place.long)
            let annotation = MKPointAnnotation()
            annotation.coordinate = coordinate
            annotation.title = place.name
            annotation.subtitle = place.subtitle
            
            mapView.addAnnotation(annotation)
        }
    }
}

