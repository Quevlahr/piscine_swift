//
//  Data.swift
//  d03
//
//  Created by Quentin ROULON on 10/3/18.
//  Copyright © 2018 Quentin ROULON. All rights reserved.
//

import Foundation

struct ImagesData    {
    static var images : [(name: String, description: String, url: String, imageCache: Any?)] = [
        ("pont", "Joli pont", "https://www.wallpaperup.com/uploads/wallpapers/2012/12/03/23633/361a12932df98f3e41568b19cc314e12-1400.jpg", nil),
        ("vague", "Jolie vague", "https://www.tuxboard.com/photos/2014/04/Plus-belle-photo-de-vague-2.jpg", nil),
        ("phare", "Joli phare", "https://www.cewe.fr/blog/wp-content/uploads/sites/8/2016/03/belle-ile-1.jpg", nil),
        ("ouragan", "Joli ouragan", "https://www.nasa.gov/sites/default/files/thumbnails/image/45025340661_7b9f8f9402_k.jpg", nil),
        ("lancement", "Joli lancement", "https://www.nasa.gov/sites/default/files/thumbnails/image/42374725534_a653d1b7cb_k.jpg", nil),
        ("can't be downloaded", "Jolie vague", "https://www.tuxboard.com/photos/2014/04/Plus-belle-photo-de-vague-.jpg", nil),
        ("bad format url", "Joli phare", "", nil),
    ]
}
