//
//  ViewController.swift
//  d03
//
//  Created by Quentin ROULON on 10/3/18.
//  Copyright © 2018 Quentin ROULON. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate {

//    @IBOutlet weak var imagesCollectionView: UICollectionView!
//    weak var imagesCollectionView: UICollectionView! {
//        self.imagesCollectionView.dataSource = self
//    }
    
    @IBOutlet weak var imageViewInVC: UIImageView!
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return ImagesData.images.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "imageCell", for: indexPath) as! ImageCollectionViewCell
        cell.displayImage(data: ImagesData.images[indexPath.row], viewController: self)
        
        return cell
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "imageSegue" {
            let imageViewController = segue.destination as! ImageViewController
            
            if let image = sender as? UIImage {
                imageViewController.imageTmp = image
            }
        }
    }
}

