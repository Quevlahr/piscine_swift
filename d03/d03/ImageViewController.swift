//
//  ImageViewController.swift
//  d03
//
//  Created by Quentin ROULON on 10/4/18.
//  Copyright © 2018 Quentin ROULON. All rights reserved.
//

import UIKit

class ImageViewController: UIViewController, UIScrollViewDelegate {

    @IBOutlet weak var imageScrollView: UIScrollView!
    var imageTmp: UIImage?
    var imageView: UIImageView?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if let image: UIImage = imageTmp {
            imageView = UIImageView(image: image)
            imageScrollView.addSubview(imageView!)
            imageScrollView.contentSize = (imageView?.frame.size)!
            imageScrollView.maximumZoomScale = 100
            imageScrollView.minimumZoomScale = fmin(self.view.frame.width / image.size.width, self.view.frame.height)
        }
    }

//    override func didRotate(from fromInterfaceOrientation: UIInterfaceOrientation) {
//        imageScrollView.minimumZoomScale = fmin(self.view.frame.width / imageTmp!.size.width, self.view.frame.height)
//        if (imageScrollView.zoomScale < imageScrollView.minimumZoomScale) {
//            imageScrollView.zoomScale = imageScrollView.minimumZoomScale
//        }
//    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        coordinator.animate(
            alongsideTransition: {
                (UIViewControllerTransitionCoordinatorContext) -> Void in
            }, completion: {
                (UIViewControllerTransitionCoordinatorContext) -> Void in
                self.imageScrollView.minimumZoomScale = fmin(self.view.frame.width / self.imageTmp!.size.width, self.view.frame.height)
                if (self.imageScrollView.zoomScale < self.imageScrollView.minimumZoomScale) {
                    self.imageScrollView.zoomScale = self.imageScrollView.minimumZoomScale
                }
            }
        )
    }
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return imageView
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
