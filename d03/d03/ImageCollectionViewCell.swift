//
//  ImageCollectionViewCell.swift
//  d03
//
//  Created by Quentin ROULON on 10/3/18.
//  Copyright © 2018 Quentin ROULON. All rights reserved.
//

import UIKit

class ImageCollectionViewCell: UICollectionViewCell {
    
    static var networkActivityCount: Int = 0
    var viewController: UIViewController?
    @IBOutlet weak var imageLabel: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    
    func enableClick() {
        imageView.isUserInteractionEnabled = true
        imageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.imageTap)))
    }
    
    @objc private func imageTap() {
        viewController?.performSegue(withIdentifier: "imageSegue", sender: imageView.image)
    }
    
    func displayImage(data: (name: String, description: String, url: String, imageCache: Any?), viewController: UIViewController)
    {
        if self.viewController == nil {
            self.viewController = viewController
        }
        imageLabel.text = data.name
        imageView.image = nil
        
        let qos = DispatchQoS.background.qosClass
        let queue = DispatchQueue.global(qos: qos)
        
        let activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView()
        activityIndicator.center = self.imageView.center
        activityIndicator.hidesWhenStopped = true
        activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.gray
        self.imageView.addSubview(activityIndicator)
        activityIndicator.startAnimating()
        
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        ImageCollectionViewCell.networkActivityCount += 1
        
        queue.async {
            if let imageUrl = URL(string: data.url) {
                if let imageData = try? Data(contentsOf: imageUrl) {
                    DispatchQueue.main.async {
                        self.imageView.image = UIImage(data: imageData)
                        activityIndicator.stopAnimating()
                        ImageCollectionViewCell.networkActivityCount -= 1
                        if (ImageCollectionViewCell.networkActivityCount == 0) {
                            UIApplication.shared.isNetworkActivityIndicatorVisible = false
                        }
                        self.enableClick()
                    }
                } else {
                    self.stopAnimatingAndIndicator(viewController: viewController, activityIndicator: activityIndicator, message: "An image can't be downloaded")
                }
            } else {
                self.stopAnimatingAndIndicator(viewController: viewController, activityIndicator: activityIndicator, message: "An url is not well formatted")
            }
        }
    }
    
    private func stopAnimatingAndIndicator(viewController: UIViewController, activityIndicator: UIActivityIndicatorView, message: String) {
        DispatchQueue.main.async {
            ImageCollectionViewCell.networkActivityCount -= 1
            activityIndicator.stopAnimating()
            let alert = UIAlertController(title: "An error occured", message: message, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
            viewController.present(alert, animated: true)
        }
    }
}

