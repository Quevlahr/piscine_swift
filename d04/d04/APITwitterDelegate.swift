//
//  APITwitterDelegate.swift
//  d04
//
//  Created by Quentin ROULON on 10/5/18.
//  Copyright © 2018 Quentin ROULON. All rights reserved.
//

import Foundation

protocol APITwitterDelegate: NSObjectProtocol {
    func processTweet(tweets: [Tweet])
    func tweetError(error: Error)
}
