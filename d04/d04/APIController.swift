//
//  APIController.swift
//  d04
//
//  Created by Quentin ROULON on 10/5/18.
//  Copyright © 2018 Quentin ROULON. All rights reserved.
//

import UIKit

class APIController {

    weak var delegate: APITwitterDelegate?
    let token: String
    
    init(delegate: APITwitterDelegate?, token: String) {
        self.delegate = delegate
        self.token = token
    }
    
    func loadTweets(search: String) {
        if self.token != "" {
            
            var url: URL?
            if let searchUrl = search.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed) {
                url = URL(string: "https://api.twitter.com/1.1/search/tweets.json?lang=fr&count=100&q=" + searchUrl)
            } else {
                url = URL(string: "https://api.twitter.com/1.1/search/tweets.json?lang=fr&count=100&q=ecole%2042")
            }
            let request = NSMutableURLRequest(url: url!)
            request.httpMethod = "GET"
            request.setValue("Bearer " + token, forHTTPHeaderField: "Authorization")
            
            let task = URLSession.shared.dataTask(with: request as URLRequest) {
                (data, response, error) in
                if error != nil {
                    self.delegate?.tweetError(error: error!)
                }
                else if let d = data {
                    do {
                        if let dic: NSDictionary = try JSONSerialization.jsonObject(with: d, options: JSONSerialization.ReadingOptions.mutableLeaves) as? NSDictionary {
                            
                            if let statuses = dic["statuses"] as? [[String:Any]] {
                                var newTweets: [Tweet] = []
                                var newData: (name: String, text: String, date: Date) = ("", "", Date())
                                for status in statuses{
                                    if let textIndex = status.index(forKey: "text") {
                                        newData.text = status[textIndex].value as! String
                                    }
                                    if let allUserIndex = status.index(forKey: "user") {
                                        if let userEntity = status[allUserIndex].value as? [String:Any] {
                                            if let userName = userEntity.index(forKey: "name") {
                                                newData.name = userEntity[userName].value as! String
                                            }
                                        }
                                    }
                                    if let creationDateIndex = status.index(forKey: "created_at") {
                                        let dateFormatter = DateFormatter()
                                        dateFormatter.dateFormat = "EEE MMM dd HH:mm:ss ZZZZZ yyyy"
                                        newData.date = dateFormatter.date(from: status[creationDateIndex].value as! String)!
                                    }
                                    newTweets.append(Tweet(name: newData.name, text: newData.text, date: newData.date))
                                }
                                self.delegate?.processTweet(tweets: newTweets)
                            }
                        }
                    }
                    catch(let err) {
                        self.delegate?.tweetError(error: err)
                    }
                }
            }
            task.resume()
            
        }
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
