//
//  ViewController.swift
//  d04
//
//  Created by Quentin ROULON on 10/5/18.
//  Copyright © 2018 Quentin ROULON. All rights reserved.
//

import UIKit

class ViewController: UIViewController, APITwitterDelegate, UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate {
    
    var twitterApiController: APIController?
    var tweets: [Tweet] = []
    @IBOutlet weak var tweetTableView: UITableView! {
        didSet {
            self.tweetTableView.delegate = self
            self.tweetTableView.dataSource = self
        }
    }
    @IBOutlet weak var searchTextField: UITextField! {
        didSet {
            self.searchTextField.delegate = self
        }
    }
    
    /*************
    * PROTOCOLS
    **************/
    
    func processTweet(tweets: [Tweet]) {
        self.tweets = tweets
        DispatchQueue.main.async {
            self.tweetTableView.reloadData()
            self.tweetTableView.endUpdates()
        }
    }
    
    func tweetError(error: Error) {
        DispatchQueue.main.async {
            let alert = UIAlertController(title: "Error", message: "An error occured", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
            self.present(alert, animated: true)
        }
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tweets.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "tweetCell") as? TweetTableViewCell
        cell?.displayCell(tweet: self.tweets[indexPath.row])
        return cell!
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        twitterApiController?.loadTweets(search: textField.text!)
        return true
    }
    
    /*************
     * OVERRIDE
     *************/
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getTokenApiTwitter()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    /*************
     * PRIVATE
     *************/
    
    private func getTokenApiTwitter() {
        let CUSTOMER_KEY = "eDOr2OcMcWm6FkRVCVaJn7uon"
        let CUSTOMER_SECRET = "Zr2unsYSDqs6fyfARpl4dTpr2EEdZH5bnoxXEoWFzXjukQu0Yn"
        
        let BEARER = ((CUSTOMER_KEY + ":" + CUSTOMER_SECRET).data(using: String.Encoding.utf8))!.base64EncodedString(options: Data.Base64EncodingOptions(rawValue:0))
        
        let url = URL(string: "https://api.twitter.com/oauth2/token")
        let request = NSMutableURLRequest(url: url!)
        request.httpMethod = "POST"
        request.setValue("Basic " + BEARER, forHTTPHeaderField: "Authorization")
        request.setValue("application/x-www-form-urlencoded;charset=UTF-8", forHTTPHeaderField: "Content-Type")
        request.httpBody = "grant_type=client_credentials".data(using: String.Encoding.utf8)
        let task = URLSession.shared.dataTask(with: request as URLRequest) {
            (data, response, error) in
            if error != nil {
                self.tweetError(error: error!)
            }
            else if let d = data {
                do {
                    if let dic: NSDictionary = try JSONSerialization.jsonObject(with: d, options: JSONSerialization.ReadingOptions.mutableLeaves) as? NSDictionary {
                        if let token = dic.object(forKey: "access_token" ) as? String {
                            self.twitterApiController = APIController(delegate: self, token: token)
                            self.twitterApiController?.loadTweets(search: "ecole 42")
                        } else {
                            DispatchQueue.main.async {
                                let alert = UIAlertController(title: "An error occured", message: "Cannot get token from Twitter's API", preferredStyle: .alert)
                                alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
                                self.present(alert, animated: true)
                            }
                        }
                    }
                }
                catch(let err) {
                    self.tweetError(error: err)
                }
            }
        }
        task.resume()
//        self.twitterApiController = APIController(delegate: self, token: "AAAAAAAAAAAAAAAAAAAAAPV08gAAAAAA0hywGN9mAViYhuX6C8EcjJw3ZqM%3DLd7pc3FBawOIKHQrz5nI1307b1Od9SUIe0FUfZsJvZQtjKHOko")
    }

}

