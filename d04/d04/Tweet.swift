//
//  Tweet.swift
//  d04
//
//  Created by Quentin ROULON on 10/5/18.
//  Copyright © 2018 Quentin ROULON. All rights reserved.
//

import Foundation

struct Tweet: CustomStringConvertible {
    var description: String {
        return "\(name), \(text)"
    }
    
    let name: String
    let text: String
    let date: Date
}
